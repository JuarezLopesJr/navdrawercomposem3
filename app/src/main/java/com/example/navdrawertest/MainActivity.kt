package com.example.navdrawertest

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.NavigationDrawerItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.navdrawertest.ui.theme.NavDrawerTestTheme
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            NavDrawerTestTheme {
                val state = rememberDrawerState(initialValue = DrawerValue.Closed)
                val scope = rememberCoroutineScope()
                val items = listOf(
                    MenuItem(
                        id = "home",
                        title = "Home",
                        contentDescription = "home",
                        icon = Icons.Default.Home
                    ),
                    MenuItem(
                        id = "settings",
                        title = "Settings",
                        contentDescription = "settings",
                        icon = Icons.Default.Settings
                    ),
                    MenuItem(
                        id = "help",
                        title = "Help",
                        contentDescription = "help",
                        icon = Icons.Default.Info
                    )
                )
                var selectedItem by remember { mutableStateOf(items[0]) }

                Scaffold(
                    topBar = {
                        MainAppBar(onNavigationIconClick = {
                            scope.launch {
                                state.open()
                            }
                        })
                    },
                    content = { innerPadding ->
                        Column(modifier = Modifier.padding(innerPadding)) {

                            ModalNavigationDrawer(
                                drawerState = state,
                                drawerContent = {
                                    Text(
                                        text = "Menu",
                                        fontWeight = FontWeight.Bold,
                                        modifier = Modifier.padding(start = 20.dp, top = 20.dp)
                                    )

                                    Spacer(modifier = Modifier.height(20.dp))

                                    items.forEach { item ->
                                        NavigationDrawerItem(
                                            icon = {
                                                Icon(
                                                    imageVector = item.icon,
                                                    contentDescription = item.contentDescription
                                                )
                                            },
                                            label = { Text(text = item.title) },
                                            selected = item == selectedItem,
                                            onClick = {
                                                scope.launch {
                                                    state.open()
                                                    selectedItem = item
                                                }
                                            },
                                            modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding)
                                        )
                                    }
                                },
                                content = {
                                    Surface(color = MaterialTheme.colorScheme.surface) {
                                        Column(
                                            modifier = Modifier
                                                .fillMaxSize()
                                                .padding(16.dp),
                                            horizontalAlignment = Alignment.CenterHorizontally,
                                            verticalArrangement = Arrangement.Center
                                        ) {
                                            Text(text = if (state.isClosed) ">>> Swipe >>>" else "<<< Swipe <<<")

                                            Spacer(Modifier.height(20.dp))

                                            Text(text = "Screen content goes here")
                                        }
                                    }
                                }
                            )
                        }
                    }
                )
            }
        }
    }
}