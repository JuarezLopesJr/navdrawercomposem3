package com.example.navdrawertest

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


data class MenuItem(
    val id: String,
    val title: String,
    val icon: ImageVector,
    val contentDescription: String
)

@Composable
fun DrawerHeader() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 64.dp),
        contentAlignment = Alignment.Center
    ) {
        Text(text = "Header test here", fontSize = 20.sp)
    }
}

@Composable
fun DrawerBody(
    modifier: Modifier = Modifier,
    items: List<MenuItem>,
    itemTextStyle: TextStyle = TextStyle(fontSize = 18.sp),
    onItemClicked: (MenuItem) -> Unit
) {
    items.forEach { item ->
        Row(
            modifier = modifier
                .fillMaxWidth()
                .clickable { onItemClicked(item) }
                .padding(all = 16.dp)
        ) {
            Icon(imageVector = item.icon, contentDescription = item.contentDescription)

            Spacer(modifier = Modifier.width(16.dp))

            Text(
                text = item.title,
                modifier = Modifier.weight(1f),
                style = itemTextStyle
            )
        }
    }
}